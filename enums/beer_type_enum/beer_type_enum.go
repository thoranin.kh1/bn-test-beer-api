package beer_type_enum

import (
	"strings"
)

type BeerType int32

const (
	PLISNER BeerType = iota
	ALE
	LAGER
	STOUT
	PORTER
	SESSION_BEER
)

var BeerTypeList = map[BeerType]string{
	PLISNER:      "PLISNER",
	ALE:          "ALE",
	LAGER:        "LAGER",
	STOUT:        "STOUT",
	PORTER:       "PORTER",
	SESSION_BEER: "SESSION_BEER",
}

func (c BeerType) String() string {
	if err, ok := BeerTypeList[c]; ok {
		return err
	}
	return ""
}

func IsValid(value string) bool {
	for _, item := range BeerTypeList {
		if strings.ToUpper(value) == item {
			return true
		}
	}
	return false
}
