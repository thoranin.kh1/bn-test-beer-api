package action_enum

type ActionTypes int32

const (
	INSERT ActionTypes = iota
	UPDATE
	DELETE
)

var ActionTypesLists = map[ActionTypes]string{
	INSERT: "INSERT",
	UPDATE: "UPDATE",
	DELETE: "DELETE",
}

func (e ActionTypes) String() string {
	if err, ok := ActionTypesLists[e]; ok {
		return err
	}
	return ""
}
