package error_enum

import "errors"

type ErrorTypes int32

const (
	MARIEDB ErrorTypes = iota
	UPLOAD
	EXISTS
	EXPTY
)

var ErrorLists = map[ErrorTypes]error{
	MARIEDB: errors.New("MARIEDB"),
	UPLOAD:  errors.New("UPLOAD"),
	EXISTS:  errors.New("EXISTS"),
	EXPTY:   errors.New("EXPTY"),
}

func (e ErrorTypes) Error() error {
	if err, ok := ErrorLists[e]; ok {
		return err
	}
	return nil
}
