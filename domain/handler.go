package domain

import "github.com/gin-gonic/gin"

type BeerHandlerInterface interface {
	Get(c *gin.Context)
	GetByID(c *gin.Context)
	Create(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type HealcheckInterface interface {
	Healcheck(c *gin.Context)
}
