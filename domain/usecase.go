package domain

import (
	"bn-test-beer-api/model"
)

type BeerUsecaseInterface interface {
	Get(filter model.BeerFilterModel) ([]model.BeerModel, error)
	GetByID(id int) (model.BeerModel, error)
	Create(create model.BeerCreateModel) (model.BeerModel, error)
	Update(update model.BeerUpdateModel) (model.BeerModel, error)
	Delete(beerID int) error
}
