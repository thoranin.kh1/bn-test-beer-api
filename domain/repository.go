package domain

import (
	"bn-test-beer-api/model"
	"mime/multipart"
)

type BeerRepositoryInterface interface {
	Get(filter model.BeerFilterModel) ([]model.BeerModel, error)
	Create(beer model.BeerModel, fileData *multipart.FileHeader) (model.BeerModel, error)
	Update(beer model.BeerModel, fileData *multipart.FileHeader) (model.BeerModel, error)
	Delete(beer model.BeerModel) error
}
