package domain

import (
	"bn-test-beer-api/model"
	"mime/multipart"
	"time"
)

type EnvironmentLibraryInterface interface {
	SetTimezone(locate string) error
	Now() time.Time
}

type LogLibraryInterface interface {
	Debug(event string, message string, payload interface{})
	Success(event string, message string, payload interface{})
	Warning(event string, message string, payload interface{})
	Error(event string, message string, payload interface{})
}

type ResponseLibraryInterface interface {
	GetResponseSuccess(message string, data interface{}) (int, model.ResponseModel)
	GetResponseCustomWithData(code int, status bool, message string, data interface{}) (int, model.ResponseModel)
	GetResponseBadRequest() (int, model.ResponseModel)
	GetResponseInternalServerError() (int, model.ResponseModel)
}

type UploadFileInterface interface {
	GenerateFileName(prefix string, fileData *multipart.FileHeader) string
	GetFileType(fileData *multipart.FileHeader) string
	UploadFile(fileData *multipart.FileHeader, fileName string) error
	RemoveFile(fileName string) error
}
