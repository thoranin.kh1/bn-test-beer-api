package usecase

import (
	"bn-test-beer-api/domain"
	"bn-test-beer-api/enums/error_enum"
	"bn-test-beer-api/model"
)

type beerUsecase struct {
	BeerRepository domain.BeerRepositoryInterface
	Environment    domain.EnvironmentLibraryInterface
}

func NewBeerUsecase(
	beerRepository domain.BeerRepositoryInterface,
	environment domain.EnvironmentLibraryInterface,
) domain.BeerUsecaseInterface {
	return &beerUsecase{
		BeerRepository: beerRepository,
		Environment:    environment,
	}
}

func (u *beerUsecase) Get(filter model.BeerFilterModel) ([]model.BeerModel, error) {
	beers, err := u.BeerRepository.Get(filter)
	if err != nil {
		return []model.BeerModel{}, err
	}
	return beers, nil
}

func (u *beerUsecase) GetByID(id int) (model.BeerModel, error) {
	if id <= 0 {
		return model.BeerModel{}, nil
	}
	beers, err := u.Get(model.BeerFilterModel{ID: id})
	if err != nil {
		return model.BeerModel{}, err
	}
	if len(beers) > 0 {
		return beers[0], nil
	}
	return model.BeerModel{}, nil
}

func (u *beerUsecase) Create(create model.BeerCreateModel) (model.BeerModel, error) {
	beers, err := u.Get(model.BeerFilterModel{
		Name: create.Name,
	})
	if err != nil {
		return model.BeerModel{}, err
	}

	if len(beers) > 0 {
		return model.BeerModel{}, error_enum.EXISTS.Error()
	}

	now := u.Environment.Now()
	data := model.BeerModel{
		Name:        create.Name,
		TypeOfBeer:  create.TypeOfBeer,
		Description: create.Description,
		CreatedAt:   now,
		UpdatedAt:   now,
	}
	beer, err := u.BeerRepository.Create(data, create.Image)
	if err != nil {
		return model.BeerModel{}, err
	}
	return beer, nil
}

func (u *beerUsecase) Update(update model.BeerUpdateModel) (model.BeerModel, error) {
	beers, err := u.Get(model.BeerFilterModel{
		NotID: []int{update.ID},
		Name:  update.Name,
	})
	if err != nil {
		return model.BeerModel{}, err
	}

	if len(beers) > 0 {
		return model.BeerModel{}, error_enum.EXISTS.Error()
	}
	now := u.Environment.Now()
	data, err := u.GetByID(update.ID)
	if err != nil {
		return model.BeerModel{}, err
	}
	if data.ID <= 0 {
		return model.BeerModel{}, error_enum.EXPTY.Error()
	}
	data.Name = update.Name
	data.TypeOfBeer = update.TypeOfBeer
	data.Description = update.Description
	data.UpdatedAt = now

	beer, err := u.BeerRepository.Update(data, update.Image)
	if err != nil {
		return model.BeerModel{}, err
	}
	return beer, nil
}

func (u *beerUsecase) Delete(beerID int) error {
	data, err := u.GetByID(beerID)
	if err != nil {
		return err
	}
	if data.ID <= 0 {
		return error_enum.EXPTY.Error()
	}

	err = u.BeerRepository.Delete(data)
	if err != nil {
		return err
	}
	return nil
}
