package repository

import (
	"bn-test-beer-api/domain"
	"bn-test-beer-api/enums/error_enum"
	"bn-test-beer-api/model"
	"context"
	"fmt"
	"mime/multipart"
	"os"

	"gorm.io/gorm"
)

type beerRepository struct {
	BeerDB *gorm.DB
	Upload domain.UploadFileInterface
}

func NewBeerRepository(beerDB *gorm.DB, upload domain.UploadFileInterface) domain.BeerRepositoryInterface {
	return &beerRepository{
		BeerDB: beerDB,
		Upload: upload,
	}
}

func (r *beerRepository) Get(filter model.BeerFilterModel) ([]model.BeerModel, error) {
	db := r.BeerDB.Model(&model.BeerModel{})
	if filter.ID > 0 {
		db.Where("id = ?", filter.ID)
	}

	if len(filter.NotID) > 0 {
		db.Where("id NOT IN ?", filter.NotID)
	}

	if filter.Name != "" {
		db.Where("name = ?", filter.Name)
	}

	if filter.NameSearch != "" {
		db.Where("name LIKE ?", "%"+filter.NameSearch+"%")
	}

	if filter.TypeOfBeer != "" {
		db.Where("type_of_beer = ?", filter.TypeOfBeer)
	}

	beers := []model.BeerModel{}
	err := db.Find(&beers).Error
	if err != nil {
		return []model.BeerModel{}, fmt.Errorf("%w : %s", error_enum.MARIEDB.Error(), err.Error())
	}
	return beers, nil
}

func (r *beerRepository) Create(beer model.BeerModel, fileData *multipart.FileHeader) (model.BeerModel, error) {
	err := r.BeerDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {

		fileName := r.Upload.GenerateFileName("IMG-", fileData)
		beer.Image = fileName
		err := tx.Create(&beer).Error
		if err != nil {
			return fmt.Errorf("%w : %s", error_enum.MARIEDB.Error(), err.Error())
		}

		err = r.Upload.UploadFile(fileData, fileName)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return model.BeerModel{}, err
	}
	beer.ImagePresent = os.Getenv("API_IMAGE_URL") + "/" + beer.Image
	return beer, nil
}

func (r *beerRepository) Update(beer model.BeerModel, fileData *multipart.FileHeader) (model.BeerModel, error) {
	err := r.BeerDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		if fileData != nil {
			fileName := r.Upload.GenerateFileName("IMG-", fileData)
			beer.ImageOld = beer.Image
			beer.Image = fileName
		}
		err := tx.Save(&beer).Error
		if err != nil {
			return fmt.Errorf("%w : %s", error_enum.MARIEDB.Error(), err.Error())
		}
		if fileData != nil {
			err = r.Upload.UploadFile(fileData, beer.Image)
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return model.BeerModel{}, err
	}
	r.Upload.RemoveFile(beer.ImageOld)
	beer.ImagePresent = os.Getenv("API_IMAGE_URL") + "/" + beer.Image
	return beer, nil
}

func (r *beerRepository) Delete(beer model.BeerModel) error {
	err := r.BeerDB.Where("id = ?", beer.ID).Delete(&model.BeerModel{}).Error
	if err != nil {
		return fmt.Errorf("%w : %s", error_enum.MARIEDB.Error(), err.Error())
	}
	r.Upload.RemoveFile(beer.Image)
	return nil
}
