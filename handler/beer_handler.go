package handler

import (
	"bn-test-beer-api/domain"
	action_enum "bn-test-beer-api/enums/action_enum"
	"bn-test-beer-api/enums/error_enum"
	"bn-test-beer-api/model"
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
)

type beerHandler struct {
	BeerUsecase domain.BeerUsecaseInterface
	Response    domain.ResponseLibraryInterface
	Applog      domain.LogLibraryInterface
}

func NewBeerHandler(
	beerUsecase domain.BeerUsecaseInterface,
	response domain.ResponseLibraryInterface,
	applog domain.LogLibraryInterface,
) domain.BeerHandlerInterface {
	return &beerHandler{
		BeerUsecase: beerUsecase,
		Response:    response,
		Applog:      applog,
	}
}

func (h *beerHandler) Get(c *gin.Context) {
	filter := model.BeerFilterModel{
		NameSearch: c.Query("name"),
	}
	beers, err := h.BeerUsecase.Get(filter)
	if err != nil {
		c.JSON(h.Response.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Response.GetResponseSuccess("", beers))
}

func (h *beerHandler) GetByID(c *gin.Context) {
	idParam := c.Param("id")
	id, _ := strconv.Atoi(idParam)

	beer, err := h.BeerUsecase.GetByID(id)
	if err != nil {
		c.JSON(h.Response.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Response.GetResponseSuccess("", beer))
}

func (h *beerHandler) Create(c *gin.Context) {
	create := model.BeerCreateModel{}
	if err := c.Bind(&create); err != nil {
		h.Applog.Warning(action_enum.INSERT.String(), err.Error(), create)
		c.JSON(h.Response.GetResponseBadRequest())
		return
	}

	image, err := c.FormFile("image")
	if err != nil {
		h.Applog.Warning(action_enum.INSERT.String(), err.Error(), create)
		c.JSON(h.Response.GetResponseBadRequest())
		return
	}
	create.Image = image

	if err := create.Validate(); err != nil {
		h.Applog.Warning(action_enum.INSERT.String(), err.Error(), create)
		c.JSON(h.Response.GetResponseBadRequest())
		return
	}

	beer, err := h.BeerUsecase.Create(create)
	if err != nil {
		if errors.Is(err, error_enum.EXISTS.Error()) {
			message := "name already exists."
			h.Applog.Warning(action_enum.INSERT.String(), message, create)
			c.JSON(h.Response.GetResponseCustomWithData(430, false, message, nil))
			return
		}
		h.Applog.Warning(action_enum.INSERT.String(), err.Error(), create)
		c.JSON(h.Response.GetResponseInternalServerError())
		return
	}
	h.Applog.Success(action_enum.INSERT.String(), "Success", create)
	c.JSON(h.Response.GetResponseSuccess("", beer))
}

func (h *beerHandler) Update(c *gin.Context) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		h.Applog.Warning(action_enum.UPDATE.String(), err.Error(), idParam)
		c.JSON(h.Response.GetResponseBadRequest())
	}
	update := model.BeerUpdateModel{}
	if err := c.Bind(&update); err != nil {
		h.Applog.Warning(action_enum.UPDATE.String(), err.Error(), update)
		c.JSON(h.Response.GetResponseBadRequest())
		return
	}
	update.ID = id

	image, _ := c.FormFile("image")
	update.Image = image

	if err := update.Validate(); err != nil {
		h.Applog.Warning(action_enum.UPDATE.String(), err.Error(), update)
		c.JSON(h.Response.GetResponseBadRequest())
		return
	}

	beer, err := h.BeerUsecase.Update(update)
	if err != nil {
		if errors.Is(err, error_enum.EXISTS.Error()) {
			message := "name already exists."
			h.Applog.Warning(action_enum.UPDATE.String(), message, update)
			c.JSON(h.Response.GetResponseCustomWithData(430, false, message, nil))
			return
		}
		h.Applog.Warning(action_enum.UPDATE.String(), err.Error(), update)
		c.JSON(h.Response.GetResponseInternalServerError())
		return
	}
	h.Applog.Success(action_enum.UPDATE.String(), "Success", update)
	c.JSON(h.Response.GetResponseSuccess("", beer))
}

func (h *beerHandler) Delete(c *gin.Context) {
	idParam := c.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		h.Applog.Warning(action_enum.DELETE.String(), err.Error(), idParam)
		c.JSON(h.Response.GetResponseBadRequest())
	}

	err = h.BeerUsecase.Delete(id)
	if err != nil {
		if errors.Is(err, error_enum.EXISTS.Error()) {
			message := "name already exists."
			h.Applog.Warning(action_enum.DELETE.String(), message, idParam)
			c.JSON(h.Response.GetResponseCustomWithData(430, false, message, idParam))
			return
		}
		h.Applog.Warning(action_enum.DELETE.String(), err.Error(), idParam)
		c.JSON(h.Response.GetResponseInternalServerError())
		return
	}
	h.Applog.Success(action_enum.DELETE.String(), "Success", idParam)
	c.JSON(h.Response.GetResponseSuccess("", nil))
}
