package handler

import (
	"bn-test-beer-api/domain"
	"bn-test-beer-api/model"
	"time"

	"github.com/gin-gonic/gin"
)

type heathcheckHandler struct {
	Environment domain.EnvironmentLibraryInterface
	Response    domain.ResponseLibraryInterface
}

func NewHeathcheckHandler(
	environment domain.EnvironmentLibraryInterface,
	response domain.ResponseLibraryInterface,
) domain.HealcheckInterface {
	return &heathcheckHandler{
		Environment: environment,
		Response:    response,
	}
}

func (h *heathcheckHandler) Healcheck(c *gin.Context) {
	data := model.HeathCheckModel{
		Service: "bn-test-beer-api",
		Version: "1.0.0-rc",
		Now:     h.Environment.Now().Format(time.RFC3339),
	}
	c.JSON(h.Response.GetResponseSuccess("", data))
}
