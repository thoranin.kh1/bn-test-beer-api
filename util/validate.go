package util

import (
	"errors"
	"mime/multipart"
	"path/filepath"
	"strings"
)

func FormValidate() {
}

func ValidateFileType(file *multipart.FileHeader, accept []string, limit int) error {

	limitByte := int64(limit * 1000000)
	if file.Size > limitByte {
		return errors.New("FILE_SIZE_NOT_ACCEPT")
	}

	fileExtension := filepath.Ext(file.Filename)
	acceptData := "|" + strings.Join(accept, "|") + "|"
	if ok := strings.Contains(acceptData, "|"+fileExtension+"|"); !ok {
		return errors.New("FILE_TYPE_NOT_ACCEPT")
	}

	return nil
}
