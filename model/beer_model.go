package model

import (
	"bn-test-beer-api/enums/beer_type_enum"
	"bn-test-beer-api/util"
	"errors"
	"mime/multipart"
	"os"
	"time"

	"gopkg.in/validator.v2"
	"gorm.io/gorm"
)

type BeerFilterModel struct {
	ID         int
	NotID      []int
	Name       string
	NameSearch string
	TypeOfBeer string
}

type BeerModel struct {
	ID           uint           `gorm:"primaryKey"`
	Name         string         `gorm:"name" json:"name"`
	TypeOfBeer   string         `gorm:"type_of_beer" json:"type"`
	Description  string         `gorm:"description" json:"description"`
	Image        string         `gorm:"image" json:"-"`
	ImagePresent string         `gorm:"-" json:"image"`
	ImageOld     string         `gorm:"-" json:"-"`
	CreatedAt    time.Time      `json:"-"`
	UpdatedAt    time.Time      `json:"-"`
	DeletedAt    gorm.DeletedAt `gorm:"index" json:"-"`
}

func (m *BeerModel) AfterFind(*gorm.DB) error {
	m.ImagePresent = os.Getenv("API_IMAGE_URL") + "/" + m.Image
	return nil
}

type BeerCreateModel struct {
	Name        string `form:"name" validate:"nonzero"`
	TypeOfBeer  string `form:"type" validate:"nonzero"`
	Description string `form:"description" json:"description"`
	Image       *multipart.FileHeader
}

func (v *BeerCreateModel) Validate() error {
	if err := validator.Validate(v); err != nil {
		return err
	}

	err := util.ValidateFileType(v.Image, []string{".jpg", ".jpeg", ".png"}, 10)
	if err != nil {
		return err
	}

	if isValid := beer_type_enum.IsValid(v.TypeOfBeer); !isValid {
		return errors.New("BEER_TYPE_NOT_ACCCEPT")
	}
	return nil
}

type BeerUpdateModel struct {
	ID          int
	Name        string `form:"name" validate:"nonzero"`
	TypeOfBeer  string `form:"type" validate:"nonzero"`
	Description string `form:"description" json:"description"`
	Image       *multipart.FileHeader
}

func (v *BeerUpdateModel) Validate() error {
	if err := validator.Validate(v); err != nil {
		return err
	}

	if v.Image != nil {
		err := util.ValidateFileType(v.Image, []string{".jpg", ".jpeg", ".png"}, 10)
		if err != nil {
			return err
		}
	}

	if isValid := beer_type_enum.IsValid(v.TypeOfBeer); !isValid {
		return errors.New("BEER_TYPE_NOT_ACCCEPT")
	}
	return nil
}
