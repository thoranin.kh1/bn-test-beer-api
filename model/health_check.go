package model

type HeathCheckModel struct {
	Service string `json:"service"`
	Version string `json:"version"`
	Now     string `json:"now"`
}
