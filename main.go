package main

import (
	"bn-test-beer-api/handler"
	"bn-test-beer-api/library"
	"bn-test-beer-api/model"
	"bn-test-beer-api/repository"
	"bn-test-beer-api/usecase"
	"bn-test-beer-api/util"
	"context"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/mongo/driver/connstring"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	// MARK : Environment
	apiUtri := os.Getenv("API_URL")
	beerDSN := os.Getenv("DATABASE_DSN")
	mongoURI := os.Getenv("LOG_MONGO")
	uploadPath := os.Getenv("UPLOAD_PATH")

	if apiUtri != "" {
		os.Setenv("API_IMAGE_URL", apiUtri+"/images")
	}

	// MARK : enviroment
	timezone := "Asia/Bangkok"
	environment := library.NewEnvironment()
	err := environment.SetTimezone(timezone)
	if err != nil {
		timezone = time.Local.String()
		environment.SetTimezone(timezone)
	}

	// MARK : database connection
	beerDB, err := gorm.Open(mysql.Open(beerDSN), &gorm.Config{
		NowFunc: environment.Now,
	})
	if err != nil {
		log.Fatal("mariadb connection failed :: ", err.Error())
	}
	if sqlDB, err := beerDB.DB(); err != nil {
		defer sqlDB.Close()
	}

	cs, err := connstring.ParseAndValidate(mongoURI)
	if err != nil {
		log.Fatal("mongodb url :: ", err.Error())
	}

	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Fatal("mongodb connection failed :: ", err.Error())
	}
	defer client.Disconnect(context.Background())

	// MARK :: Migreate
	beerDB.AutoMigrate(&model.BeerModel{})

	// MARK : Library
	upload := library.NewUploadLibrary(uploadPath)
	if ok, err := util.PathExists(uploadPath); !ok {
		log.Fatal("upload path failed :: error = ", err)
	}
	applog := library.NewLogLibrary(client, cs.Database)
	response := library.NewResponse()

	// MARK : Module Beer
	beerRepository := repository.NewBeerRepository(beerDB, upload)
	beerUsecase := usecase.NewBeerUsecase(beerRepository, environment)
	beerHandler := handler.NewBeerHandler(beerUsecase, response, applog)

	// MARK : Module Heathcheck
	heathcheckHander := handler.NewHeathcheckHandler(environment, response)

	r := gin.Default()

	r.GET("/", heathcheckHander.Healcheck)
	r.Static("/images", uploadPath)
	beer := r.Group("beer")
	{
		beer.GET("", beerHandler.Get)
		beer.GET("/:id", beerHandler.GetByID)
		beer.POST("", beerHandler.Create)
		beer.PUT("/:id", beerHandler.Update)
		beer.DELETE("/:id", beerHandler.Delete)
	}

	r.Run(":80")

}
