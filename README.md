# Golang Beer REST API 

```bash
# Build and Run
cd bn-test-beer-api
go build
./bn-test-beer-api

# API Endpoint : http://127.0.0.1
```

## Environment
| Name | Example value |  Descript |
| --- | --- | --- |
|API_URL| http://127.0.0.1 | Image URL |
| DATABASE_DSN | root:pass1234@tcp(localhost:3306)/beerdb?charset=utf8mb4&parseTime=True&loc=Local | Marie  DB Connection String
| LOG_MONGO | mongodb://localhost:27017/log_db | MongoDB Connection String
| UPLOAD_PATH | /Users/3ikk/images | path for upload |

## API Specification
[postman collection](https://galactic-rocket-438565.postman.co/workspace/TEST~ad208969-5736-4f78-a8bf-840cb4329bee/collection/2982819-6b966ecb-ffbf-421d-b7f9-8ae7f21c782f?action=share&creator=2982819)
##
1 Heath check 

GET : /

Example Response Body 

```json
{
    "status": true,
    "code": 200,
    "data": {
        "service": "bn-test-beer-api",
        "version": "1.0.0-rc",
        "now": "2024-02-07T02:15:04+07:00"
    }
}  
```
    
| Return | Type |  Description |
| --- | --- | --- |
| status | boolean | Api status |
| code | int | Return code to show result status via http response code |
| message | string | Result's description if status = false |
| data.service | string | service name |
| data.version | string | service version |
| data.now | string | service datetime now |
    
2 Get beers <br/>
GET : /beer <br/>
<br/>
Query Params <br/>

| key | Type |  Description |
| --- | --- | --- |
| name | string | filter beer by name |

<br/>
Example Response Body <br/>

``` json
{
    "status": true,
    "code": 200,
    "data": [
        {
            "ID": 3,
            "name": "เบียร์เบลเยียม",
            "type": "PLISNER",
            "description": "มีลักษณะสีทองแดงอ่อน มีรสชาติเบา สดชื่น และคล้ายกลิ่นของมลทิน",
            "image": "http://127.0.0.1/images/IMG-bc6f7c67644851d4681896dfd165b8e3.jpg"
        }
    ]
}
```

| Return | Type |  Description |
| --- | --- | --- |
| status | boolean | Api status |
| code | int | Return code to show result status via http response code <br> [200] success <br> [500] Internal server error   |
| message | string | Result's description if status = false|
| data.ID | int | beer id |
| data.name | string | beer name |
| data.type | string | beer type |
| data.description | string | beer description |
| data.image | string | image url |

3  Get beers by ID <br/>
GET : /beer/:id <br/>
<br/>
Path parameter <br/>

| key | Type |  Description |
| --- | --- | --- |
| id | int | beer ID |

<br/>
Example Response Body <br/>

``` json
{
    "status": true,
    "code": 200,
    "data": {
        "ID": 3,
        "name": "เบียร์เบลเยียม",
        "type": "PLISNER",
        "description": "มีลักษณะสีทองแดงอ่อน มีรสชาติเบา สดชื่น และคล้ายกลิ่นของมลทิน",
        "image": "http://127.0.0.1/images/IMG-bc6f7c67644851d4681896dfd165b8e3.jpg"
    }
}
```

| Return | Type |  Description |
| --- | --- | --- |
| status | boolean | Api status |
| code | int | Return code to show result status via http response code  <br> [200] success <br> [500] Internal server error  |
| message | string | Result's description if status = false  |
| data.ID | int | beer id |
| data.name | string | beer name |
| data.type | string | beer type |
| data.description | string | beer description |
| data.image | string | image url |

4 Create beers <br/>
POST : /beer <br/>
<br/>

Request Body <b>[form-data]</b>

| Key | Type |  Description | Required | 
| --- | --- | --- | --- |
| name | Text | beer name | / |
| type | Text | beer type,  <br> {PLISNER, ALE, LAGER, STOUT, PORTER, SESSION_BEER}|  / |
| description | Text | beer description |  |
| image | File | beer image <br> limit size 10mb <be> type {jpg, jpeg, png} |  / |

Example Response Body <br/>

``` json
{
    "status": true,
    "code": 200,
    "data": {
        "ID": 3,
        "name": "เบียร์เบลเยียม",
        "type": "PLISNER",
        "description": "มีลักษณะสีทองแดงอ่อน มีรสชาติเบา สดชื่น และคล้ายกลิ่นของมลทิน",
        "image": "http://127.0.0.1/images/IMG-bc6f7c67644851d4681896dfd165b8e3.jpg"
    }
}
```

| Return | Type |  Description |
| --- | --- | --- |
| status | boolean | Api status |
| code | int | Return code to show result status via http response code <br> [200] success<br> [400] Invalid Input.   <br> [430] name already exists. <br> [500] Internal server error |
| message | string | Result's description if status = false  |
| data.ID | int | beer id |
| data.name | string | beer name |
| data.type | string | beer type |
| data.description | string | beer description |
| data.image | string | image url |

5. Update beers <br/>
PUT : /beer/:id <br/>
<br/>
Path parameter <br/>

| key | Type |  Description |
| --- | --- | --- |
| id | int | beer ID |

Request Body <b>[form-data]</b>

| Key | Type |  Description | Required | 
| --- | --- | --- | --- |
| name | Text | beer name | / |
| type | Text | beer type,  <br> {PLISNER, ALE, LAGER, STOUT, PORTER, SESSION_BEER}|  / |
| description | Text | beer description |  |
| image | File | beer image <br> limit size 10mb <be> type {jpg, jpeg, png} |   |

Example Response Body <br/>

``` json
{
    "status": true,
    "code": 200,
    "data": {
        "ID": 3,
        "name": "เบียร์เบลเยียม",
        "type": "PLISNER",
        "description": "มีลักษณะสีทองแดงอ่อน มีรสชาติเบา สดชื่น และคล้ายกลิ่นของมลทิน",
        "image": "http://127.0.0.1/images/IMG-bc6f7c67644851d4681896dfd165b8e3.jpg"
    }
}
```

| Return | Type |  Description |
| --- | --- | --- |
| status | boolean | Api status |
| code | int | Return code to show result status via http response code <br> [200] success <br> [430] name already exists. <br> [500] Internal server error |
| message | string | Result's description if status = false  |
| data.ID | int | beer id |
| data.name | string | beer name |
| data.type | string | beer type |
| data.description | string | beer description |
| data.image | string | image url |

6. Delete beers 

Delete : /beer/:id 

Path parameter 

| key | Type |  Description |
| --- | --- | --- |
| id | int | beer ID |

Example Response Body <br/>

``` json
{
    "status": true,
    "code": 200,
}
```

| Return | Type |  Description |
| --- | --- | --- |
| status | boolean | Api status |
| code | int | Return code to show result status via http response code <br> [200] success <br> [500] Internal server error  |
