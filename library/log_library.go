package library

import (
	"bn-test-beer-api/domain"
	"context"
	"os"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// MARK :: Hook Mongo
type MongoHook struct {
	Client      *mongo.Client
	MongoDBName string
}

func NewMongoHook(client *mongo.Client, mongoDBName string) *MongoHook {
	return &MongoHook{
		Client:      client,
		MongoDBName: mongoDBName,
	}
}

func (h *MongoHook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.InfoLevel,
		logrus.WarnLevel,
		logrus.ErrorLevel,
	}
}

func (h *MongoHook) Fire(e *logrus.Entry) error {
	event := ""
	if v, ok := e.Data["event"]; ok {
		event = v.(string)
	}
	status := false
	if v, ok := e.Data["status"]; ok {
		status = v.(bool)
	}
	var payload interface{}
	if v, ok := e.Data["payload"]; ok {
		payload = v
	}

	coll := h.Client.Database(h.MongoDBName).Collection("app_log")
	coll.InsertOne(context.Background(), bson.D{
		{Key: "event", Value: event},
		{Key: "status", Value: status},
		{Key: "payload", Value: payload},
		{Key: "message", Value: e.Message},
		{Key: "log_level", Value: e.Level.String()},
	})
	return nil
}

// MARK :: Log

type logLibrary struct {
	Log *logrus.Logger
}

func NewLogLibrary(mongoClient *mongo.Client, mongoDBName string) domain.LogLibraryInterface {
	log := logrus.New()
	log.Out = os.Stdout

	if err := mongoClient.Ping(context.Background(), readpref.Primary()); err == nil {
		mongoHook := NewMongoHook(mongoClient, mongoDBName)
		log.AddHook(mongoHook)
	}

	return &logLibrary{
		Log: log,
	}
}

func (l *logLibrary) MapLogField(event string, status bool, payload interface{}) logrus.Fields {
	return logrus.Fields{"event": event, "status": status, "payload": payload}
}

func (l *logLibrary) Debug(event string, message string, payload interface{}) {
	fields := l.MapLogField(event, true, payload)
	l.Log.WithFields(fields).Debug(message)
}

func (l *logLibrary) Success(event string, message string, payload interface{}) {
	fields := l.MapLogField(event, true, payload)
	l.Log.WithFields(fields).Info(message)
}

func (l *logLibrary) Warning(event string, message string, payload interface{}) {
	fields := l.MapLogField(event, false, payload)
	l.Log.WithFields(fields).Warning(message)
}

func (l *logLibrary) Error(event string, message string, payload interface{}) {
	fields := l.MapLogField(event, false, payload)
	l.Log.WithFields(fields).Error(message)
}
