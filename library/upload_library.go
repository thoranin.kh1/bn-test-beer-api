package library

import (
	"bn-test-beer-api/domain"
	"bn-test-beer-api/enums/error_enum"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
)

type uploadLibrary struct {
	UploadPath string
}

func NewUploadLibrary(uploadPath string) domain.UploadFileInterface {
	return &uploadLibrary{
		UploadPath: uploadPath,
	}
}

func (l *uploadLibrary) GenerateFileName(prefix string, fileData *multipart.FileHeader) string {
	randBytes := make([]byte, 16)
	rand.Read(randBytes)
	fileType := l.GetFileType(fileData)
	return prefix + hex.EncodeToString(randBytes) + fileType
}

func (l *uploadLibrary) GetFileType(fileData *multipart.FileHeader) string {
	fileExtension := filepath.Ext(fileData.Filename)
	return fileExtension
}

func (u *uploadLibrary) UploadFile(fileData *multipart.FileHeader, fileName string) error {
	src, err := fileData.Open()
	if err != nil {
		return fmt.Errorf("%w : %s", error_enum.UPLOAD.Error(), err.Error())
	}
	defer src.Close()

	out, err := os.OpenFile(filepath.Join(u.UploadPath, fileName), os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return fmt.Errorf("%w : %s", error_enum.UPLOAD.Error(), err.Error())
	}

	_, err = io.Copy(out, src)
	if err != nil {
		return fmt.Errorf("%w : %s", error_enum.UPLOAD.Error(), err.Error())
	}

	return nil
}

func (u *uploadLibrary) RemoveFile(fileName string) error {
	err := os.Remove(filepath.Join(u.UploadPath, fileName))
	if err != nil {
		return fmt.Errorf("%w : %s", error_enum.UPLOAD.Error(), err.Error())
	}
	return nil
}
