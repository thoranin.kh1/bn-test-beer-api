package library

import (
	"bn-test-beer-api/domain"
	"bn-test-beer-api/model"
	"net/http"
)

type responseLibrary struct{}

func NewResponse() domain.ResponseLibraryInterface {
	return &responseLibrary{}
}

func (l *responseLibrary) MapResponse(code int, status bool, message string, data interface{}) model.ResponseModel {
	return model.ResponseModel{
		Status:  status,
		Code:    code,
		Message: message,
		Data:    data,
	}
}

func (l *responseLibrary) GetResponseSuccess(message string, data interface{}) (int, model.ResponseModel) {
	return http.StatusOK, l.MapResponse(http.StatusOK, true, message, data)
}

func (l *responseLibrary) GetResponseCustomWithData(code int, status bool, message string, data interface{}) (int, model.ResponseModel) {
	return code, l.MapResponse(code, status, message, data)
}

func (l *responseLibrary) GetResponseBadRequest() (int, model.ResponseModel) {
	return http.StatusBadRequest, l.MapResponse(http.StatusBadRequest, false, "Bad request.", nil)
}

func (l *responseLibrary) GetResponseInternalServerError() (int, model.ResponseModel) {
	return http.StatusInternalServerError, l.MapResponse(http.StatusInternalServerError, false, "Internal server error.", nil)
}
