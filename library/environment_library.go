package library

import (
	"bn-test-beer-api/domain"
	"time"
)

type environmentLibrary struct{}

func NewEnvironment() domain.EnvironmentLibraryInterface {
	return &environmentLibrary{}
}

func (l *environmentLibrary) SetTimezone(locate string) error {
	loc, err := time.LoadLocation(locate)
	if err != nil {
		return err
	}
	time.Local = loc
	return nil
}

func (l *environmentLibrary) Now() time.Time {
	return time.Now()
}
